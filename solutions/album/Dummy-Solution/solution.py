from album.runner.api import setup, get_args


env_file = """name: Dummy-Solution
channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.8
  - pip

"""


def install():
    from album.runner.api import setup, get_args , get_package_path
    print("HI I AM A DUMMY INSTALL FUNCTION")


def run():
    from album.runner.api import setup, get_args , get_package_path
    from pathlib import Path
    import os
    print("HI I AM A DUMMY RUN FUNCTION")


def prepare_test():
    return {}


def test():
    from album.runner.api import setup, get_args
    import os
    print("TEST")

def close():
    from album.runner.api import setup, get_args
    print("closing")


setup(
    group="album",
    name="Dummy-Solution",
    version="0.2.0",
    title="A Dummy Solution to run nothing",
    description="A Dummy Solution to run nothing.",
    authors=["Lucas Rieckert"],
    cite=[{
        "text": "Your first citation text",
        "doi": "your first citation doi"
    }],
    tags=["vascuec", "test"],
    license="UNLICENSE",
    documentation="",
    covers=[{
        "description": "Dummy cover image.",
        "source": "cover.png"
    }],
    album_api_version="0.4.1",
    args=[],
    install=install,
    run=run,
    close=close,
    pre_test=prepare_test,
    test=test,
    dependencies={'environment_file': env_file}
)